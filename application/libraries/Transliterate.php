<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transliterate
{
  protected     $ci;

    public function __construct()
    {
        $this->ci =& get_instance();
    }

    public function transliterate($string)
    {
        $array1 = array( "á", "à", "â", "ã", "ä", "é", "è", "ê", "ë", "í", "ì", "î", "ï", "ó", "ò", "ô", "õ", "ö", "ú", "ù", "û", "ü", "ç",
                         "Á", "À", "Â", "Ã", "Ä", "É", "È", "Ê", "Ë", "Í", "Ì", "Î", "Ï", "Ó", "Ò", "Ô", "Õ", "Ö", "Ú", "Ù", "Û", "Ü", "Ç",
                         " " ); 
        $array2 = array( "a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "o", "u", "u", "u", "u", "c",
                         "A", "A", "A", "A", "A", "E", "E", "E", "E", "I", "I", "I", "I", "O", "O", "O", "O", "O", "U", "U", "U", "U", "C",
                         "-"); 
        
        $string = str_replace( $array1, $array2, $string);
        $string = preg_replace('/--+/', '-', $string);
        
        return strtolower($string);
    }

}

/* End of file Transliterate.php */
/* Location: ./application/libraries/Transliterate.php */
